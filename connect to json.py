import requests, json
s = requests.Session()

data_req = {"name":"write-your-name-here"}
head = {"Content-Type":"application/json"}
reg_resp = s.post("https://we6.talentsprint.com/game/register",headers = head, data = json.dumps(data_req))
id = reg_resp.json()['id'] 
#print(id)

create_data = {"id": id,"overwrite": True}
create_resp = s.post("https://we6.talentsprint.com/game/create", headers = head, data = json.dumps(create_data))
#print(create_resp.json())

word = {"guess":'SPEAR', "id": id}
guess_resp = s.post("https://we6.talentsprint.com/game/guess",headers = head, data = json.dumps(word)) 
print(guess_resp.json())